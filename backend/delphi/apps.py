from django.apps import AppConfig


class DelphiConfig(AppConfig):
    name = 'delphi'

    def ready(self):
        from . import signals  # noqa
