from rest_framework import serializers

from .models import OracleInquiry, OracleAnswer, Comment


class TimeStampedSerializerMixin:
    created = serializers.DateTimeField(read_only=True)
    modified = serializers.DateTimeField(read_only=True)


class OracleInquirySerializer(serializers.ModelSerializer, TimeStampedSerializerMixin):
    answer = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = OracleInquiry
        fields = ('id', 'question', 'answer', 'created', 'modified')


class OracleAnswerSerializer(serializers.ModelSerializer, TimeStampedSerializerMixin):
    answer = serializers.CharField(read_only=True)
    answer_image = serializers.URLField(read_only=True)

    class Meta:
        model = OracleAnswer
        fields = ('id', 'answer', 'answer_image', 'created', 'modified')


class CommentSerializer(serializers.ModelSerializer, TimeStampedSerializerMixin):
    class Meta:
        model = Comment
        fields = ('id', 'username', 'text', 'question', 'created', 'modified')
