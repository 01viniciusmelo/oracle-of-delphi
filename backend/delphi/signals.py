from django.db.models.signals import post_save
from django.dispatch import receiver
import django_rq

from delphi.models import OracleInquiry
from delphi.oracle import add_answer_to_inquiry


@receiver(post_save, sender=OracleInquiry)
def fetch_answer_for_inquiry(sender, instance, created, **kwargs):
    if created:
        django_rq.enqueue(add_answer_to_inquiry, instance.id)
