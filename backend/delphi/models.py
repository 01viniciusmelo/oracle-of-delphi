from django.db import models
from django.utils.translation import gettext_lazy as _


class TimeStampedBaseModel(models.Model):
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        abstract = True


class OracleInquiry(TimeStampedBaseModel):
    question = models.TextField(_('question'))
    answer = models.OneToOneField('OracleAnswer', verbose_name=_('answer'), null=True, related_name='inquiry')

    def __str__(self):
        return self.question[:30]

    class Meta:
        verbose_name = _('Inquiry')
        verbose_name_plural = _('Inquiries')


class OracleAnswer(TimeStampedBaseModel):
    YES = 'yes'
    NO = 'no'
    MAYBE = 'maybe'
    ANSWER_CHOICES = (
        (YES, _('yes')),
        (NO, _('no')),
        (MAYBE, _('maybe')),
    )
    answer = models.CharField(_('answer'), choices=ANSWER_CHOICES, max_length=5)
    answer_image = models.URLField(_('image'))

    def __str__(self):
        return self.answer[:30]

    class Meta:
        verbose_name = _('Answer')
        verbose_name_plural = _('Answers')


class Comment(TimeStampedBaseModel):
    username = models.CharField(_('username'), max_length=20)
    text = models.TextField(_('text'))
    question = models.ForeignKey('OracleInquiry', verbose_name=_('question'), related_name='comments')

    def __str__(self):
        return self.answer[:30]
