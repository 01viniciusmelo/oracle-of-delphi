from rest_framework import routers

from .views import OracleInquiryViewSet, OracleAnswerViewSet, CommentViewSet

router = routers.DefaultRouter()
router.register(r'questions', OracleInquiryViewSet)
router.register(r'answers', OracleAnswerViewSet)
router.register(r'comments', CommentViewSet)

urlpatterns = router.urls
