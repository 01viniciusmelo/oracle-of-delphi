from delphi import models


def test_question_can_be_created_without_answer(db):
    inquiry = models.OracleInquiry()
    inquiry.question = 'Will this test pass?'
    inquiry.save()
    assert models.OracleInquiry.objects.get(id=inquiry.id) == inquiry


def test_answer_creation_works(db):
    answer = models.OracleAnswer()
    answer.answer_image = 'https://yesno.wtf/assets/yes/12-e4f57c8f172c51fdd983c2837349f853.gif'
    answer.answer = 'yes'
    answer.save()
    assert models.OracleAnswer.objects.get(id=answer.id) == answer
