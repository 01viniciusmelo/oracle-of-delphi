def test_get_answer_calls_get_with_url(mocker):
    import time
    import requests
    mocker.patch.object(time, 'sleep')
    get_mock = mocker.patch.object(requests, 'get')

    from delphi.oracle import get_answer
    get_answer()
    get_mock.assert_called_with('https://yesno.wtf/api/')
