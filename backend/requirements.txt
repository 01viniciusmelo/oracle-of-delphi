#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file requirements.txt requirements.in
#
appdirs==1.4.2            # via setuptools
click==6.7                # via rq
django-environ==0.4.1
django-filter==1.0.1
django-rq==0.9.4
Django==1.10.6            # via django-environ, django-rq
djangorestframework==3.5.4
flake8==3.3.0             # via pytest-flake8
markdown==2.6.8
mccabe==0.6.1             # via flake8
packaging==16.8           # via setuptools
psycopg2==2.7
py==1.4.32                # via pytest
pycodestyle==2.3.1        # via flake8
pyflakes==1.5.0           # via flake8
pyparsing==2.1.10         # via packaging
pytest-django==3.1.2
pytest-flake8==0.8.1
pytest-mock==1.5.0
pytest==3.0.6
redis==2.10.5             # via rq
requests==2.13.0
rq==0.7.1                 # via django-rq
six==1.10.0               # via django-environ, packaging, setuptools

# The following packages are considered to be unsafe in a requirements file:
# setuptools                # via pytest
