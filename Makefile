.PHONY: all
all: help

.PHONY: help
help:
	@echo frequently used:
	@echo "\t"make requirements-upgrade"                           "- upgrade python requirements
	@echo "\t"make build"                                          "- create a new build
#	@echo "\t"make release"                                        "- make a new release
	@echo
	@echo available targets:
	@$(MAKE) --no-print-directory list

.PHONY: list
list:
	@$(MAKE) --no-print-directory _list_targets_on_separate_lines | sed -e 's/^/\t/'

.PHONY: _list_targets_on_separate_lines
_list_targets_on_separate_lines:
# Adopted from http://stackoverflow.com/a/26339924/674064
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | \
	    awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | \
	    sort | \
	    egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: build-frontend
build-frontend:
	docker-compose build frontend && \
	docker-compose run --rm frontend bash -c "npm run build && find ./dist -type d -exec chmod 777 {} \; && find ./dist -type f -exec chmod 666 {} \;" && \
	docker-compose build frontend

.PHONY: build-backend
build-backend:
	docker-compose build backend backend-setup backend-rq

.PHONY: build-nginx
build-nginx:
	docker-compose build nginx

.PHONY: build
build: build-frontend build-backend build-nginx

.PHONY: requirements-upgrade
requirements-upgrade: build-backend
	docker-compose run --rm backend bash -c "cd /opt/backend/ && pip-compile -U requirements.in && chmod 666 requirements.txt"

.PHONY: tests
tests: build-backend
	docker-compose run --rm backend pytest -q --flake8
	docker-compose run --rm backend pytest
